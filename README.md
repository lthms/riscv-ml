# `riscv-ml`

Extracted from https://github.com/riscv/sail-riscv

```
opam switch create . 4.14.0
opam install lem linksem dune
dune build
```
